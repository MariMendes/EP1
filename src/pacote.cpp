#include "pacote.hpp"
#include "crypto.hpp"
#include "array.hpp"

#include <iostream>

typedef union tamanho_pacote{
	int tamanho;
	byte tamanho_byte[4];
}TamanhoPacote;

typedef union tamanho_pacote_interno{
	int tamanho;
	byte tamanho_byte[2];
}TamanhoInterno;

Pacote::Pacote(byte tag){
	pacote = array::create(7);
	pacote -> data[0] = 0x03;
	pacote -> data[1] = 0;
	pacote -> data[2] = 0;
	pacote -> data[3] = 0;
	pacote -> data[4] = tag;
	pacote -> data[5] = 0;
	pacote -> data[6] = 0;
}

Pacote::Pacote(byte tag, array::array *value){
	array::array *hash;
	pacote = array::create((int)value->length + 27);	
	hash = crypto::sha1(value);
	TamanhoPacote tamanho_pacote;
	TamanhoInterno tamanho_interno; 
	tamanho_pacote.tamanho = 23+(int)value->length;
	tamanho_interno.tamanho = (int)value->length;
	pacote->data[0] = tamanho_pacote.tamanho_byte[0];
	pacote->data[1] = tamanho_pacote.tamanho_byte[1];
	pacote->data[2] = tamanho_pacote.tamanho_byte[2];
	pacote->data[3] = tamanho_pacote.tamanho_byte[3];
	pacote->data[4] = tag;
	pacote->data[5] = tamanho_interno.tamanho_byte[0];
	pacote->data[6] = tamanho_interno.tamanho_byte[1];
	
	memcpy(pacote->data + 7, value->data, value->length);
	memcpy(pacote->data + 7 + value->length, hash->data, 20);
}

Pacote::Pacote(array::array *pacote_recebido, byte tag){
	byte tag_recebido;
	array::array *length_recebido = array::create(2);
	array::array *value_recebido;
	array::array *hash_recebido = array::create(20);
	if(pacote_recebido->data[0] != tag){}
	else{
		tag_recebido = pacote_recebido->data[0];
		length_recebido->data[0] = pacote_recebido->data[1];
		length_recebido->data[1] = pacote_recebido->data[2];
		TamanhoInterno tamanho_interno;
		tamanho_interno.tamanho_byte[0] = length_recebido->data[0];
		tamanho_interno.tamanho_byte[1] = length_recebido->data[1];

		value_recebido = array::create(tamanho_interno.tamanho);
		for(int i = 3, j=0; i < (tamanho_interno.tamanho+3), j< tamanho_interno.tamanho;i++, j++){
			value_recebido->data[j] = pacote_recebido->data[i];
		}
		for(int i = tamanho_interno.tamanho, f = 0; i < (tamanho_interno.tamanho + 20), f < 20; i++, f++){
			hash_recebido->data[f] = pacote_recebido->data[i];
		}
	this->value = value_recebido;
	}
}

array::array *Pacote::getPacote(){
	return pacote;
}
array::array *Pacote::getValue(){
	return value;
}