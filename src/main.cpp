#include "array.hpp"
#include "network.hpp"
#include "pacote.hpp"
#include "crypto.hpp"

#include <iostream>
#include <iomanip>
#include <ostream>
#include <istream>
#include <unistd.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <fstream>

using namespace std;

#define REQUEST_REGISTRATION 0xC0
#define REGISTRATION_START 0xC1
#define REGISTER_ID 0xC2
#define REGISTERED 0xC3

#define REQUEST_AUTH 0xA0 
#define AUTH_START 0xA1
#define REQUEST_CHALLENGE 0xA2
#define CHALLENGE 0xA4  
#define AUTHENTICATE 0xA5
#define AUTHENTICATED 0xA6

#define REQUEST_OBJECT 0xB0
#define OBJECT 0xB1

#define NOT_AUTHENTICATED 0xE0
#define OBJECT_NOT_FOUND 0xE1
#define INVALID_KEY 0xE2
#define ALREADY_REGISTERED 0xE3
#define FAILED_REGISTRATION 0xE4
#define NOT_AUTHORIZED 0xEF
#define SERVER_ERROR 0xE5

union tamanho_pacote{
	int tamanho;
	byte tamanho_byte[4];
};

typedef union tamanho_interno{
	int tamanho;
	byte tamanho_byte[2];
}TamanhoInterno;

int main(int argc, char const *argv[]){
	int fd;
	array::array *pacote_registration_start;
	array::array *pacote_registered;
	array::array *pacote_auth_start;
	array::array *pacote_challenge;
	array::array *pacote_authenticated;
	array::array *pacote_objeto;
	array::array *hash;
	array::array *value_registered;
	array::array *token_A_descriptado;
	array::array *chave_S_descriptada;
	array::array *M_descriptado;
	array::array *cabecalho;
	array::array *token_T_descriptado;
	array::array *objeto_descriptado;

	array::array *ID = array::create(8);
	ID->data[0] = 0x93;
	ID->data[1] = 0x3F;
	ID->data[2] = 0x50;
	ID->data[3] = 0x66;
	ID->data[4] = 0xDB;
	ID->data[5] = 0xB5;
	ID->data[6] = 0xC6;
	ID->data[7] = 0xCE;

	array::array *ID_objeto = array::create(8);
	ID_objeto->data[0] = 0x01;     
	ID_objeto->data[1] = 0x02;     
	ID_objeto->data[2] = 0x03;     
	ID_objeto->data[3] = 0x04;     
	ID_objeto->data[4] = 0x05;     
	ID_objeto->data[5] = 0x06;     
	ID_objeto->data[6] = 0x07;     
	ID_objeto->data[7] = 0x08;     

	RSA *chave_privada_cliente = crypto::rsa_read_private_key_from_PEM("Files/private.pem");
	RSA *chave_publica_servidor  = crypto::rsa_read_public_key_from_PEM("Files/server_pk.pem");

	array::array *ID_criptado = crypto::rsa_encrypt(ID, chave_publica_servidor);

	Pacote *pacote_request_registration;
	pacote_request_registration = new Pacote(REQUEST_REGISTRATION);
	Pacote *pacote_registration_start_auth;
	Pacote *pacote_register_id;
	Pacote *pacote_registered_auth;
	Pacote *pacote_request_auth;
	Pacote *pacote_token;
	Pacote *pacote_auth_start_auth;
	Pacote *pacote_request_challenge;
	Pacote *pacote_challenge_auth;
	Pacote *pacote_authenticate;
	Pacote *pacote_authenticated_auth;
	Pacote *pacote_request_object;
	Pacote *pacote_objeto_auth;

	union tamanho_pacote pacote_registered_union;
	union tamanho_pacote pacote_auth_start_union;
	union tamanho_pacote pacote_challenge_union;
	union tamanho_pacote pacote_authenticated_union;
	union tamanho_pacote pacote_objeto_union;

	printf("\n\n");
	cout <<  "\t\tEP1 - Orientação a Objetos" << endl;
	cout <<	 "\t\tProfessor: Renato Sampaio Coral" << endl;
	cout <<	 "\t\tAluna: Mariana de Souza Mendes" << endl;
	cout <<	 "\t\tMatricula: 14/0154027" << endl;
	printf("\n\n");
	cout << "Implementação de um cliente seguro, com uso de criptografica (simétrica e assimétrica) para envio de imagens" << endl;
	printf("\n\n");
	cout << "CLIENTE SEGURO - Pronto para comunicação" << endl; 
	printf("\n\n");
	cout << "Iniciando a comunicação com o servidor" <<endl;

	fd = network::connect("45.55.185.4", 3000);
	if(fd < 0){
		cout << "Falha na conexão" << fd << endl;
	}else{
		cout << "Conexão OK!" << endl;
		
		cout << "PROTOCOLO DE REGISTRO" << endl;
		network::write(fd, pacote_request_registration->getPacote());
		if((pacote_registration_start = network::read(fd))== nullptr){
			cout << "Leitura nula" << endl;
		}else{
			pacote_registration_start_auth = new Pacote(pacote_registration_start, REGISTRATION_START);
			if(pacote_registration_start_auth->getPacote() == NULL){
				cout << "Error, hash ou tag diferentes" << endl; 
			}else{
				cout << "Pacote esperado recebido com sucesso!" << endl;	
			}
			pacote_register_id = new Pacote(REGISTER_ID, ID_criptado);
			sleep(2);
			printf("\n");
			network::write(fd, pacote_register_id->getPacote());

			union tamanho_pacote pacote_registered_union;
			if((cabecalho = network::read(fd, 4)) == nullptr){
				cout << "Erro na leitura do pacote" << endl;
			}else{
				pacote_registered_union.tamanho_byte[0] = cabecalho->data[0];
				pacote_registered_union.tamanho_byte[1] = cabecalho->data[1];
				pacote_registered_union.tamanho_byte[2] = cabecalho->data[2];
				pacote_registered_union.tamanho_byte[3] = cabecalho->data[3];
				if((pacote_registered = network::read(fd, pacote_registered_union.tamanho)) == nullptr){
					cout << "Erro na leitura do pacote" << endl;	
				}else{
					cout << "Imprimindo conteudo do pacote recebido REGISTERED: " << *pacote_registered << endl;
					sleep(2);
					printf("\n");
					pacote_registered_auth = new Pacote(pacote_registered, REGISTERED);
					cout << "pacote registered " << *pacote_registered_auth->getValue() << endl;
					array::array *chave_S = pacote_registered_auth->getValue();
					chave_S_descriptada = crypto::rsa_decrypt(chave_S, chave_privada_cliente);
				}
			}
		}
	}
	printf("\n\n\n");
	cout << "PROTOCOLO DE AUTENTICAÇÃO" << endl;
	pacote_request_auth = new Pacote(REQUEST_AUTH, ID_criptado);
	network::write(fd, pacote_request_auth->getPacote());

	array::destroy(cabecalho);
	if((cabecalho = network::read(fd, 4)) == nullptr){
		cout << "Erro na leitura do pacote" << endl;
	}else{
		pacote_auth_start_union.tamanho_byte[0] = cabecalho->data[0];
		pacote_auth_start_union.tamanho_byte[1] = cabecalho->data[1];
		pacote_auth_start_union.tamanho_byte[2] = cabecalho->data[2];
		pacote_auth_start_union.tamanho_byte[3] = cabecalho->data[3];

		if((pacote_auth_start = network::read(fd, pacote_auth_start_union.tamanho)) == nullptr){
			cout << "Erro na leitura do pacote" << endl;	
		}
else{
			cout << "Imprimindo conteudo do pacote recebido AUTH_START: " << *pacote_auth_start << endl;
			sleep(2);
			printf("\n");
			pacote_auth_start_auth = new Pacote(pacote_auth_start, AUTH_START);
			array::array *token_A = pacote_auth_start_auth->getValue();
			token_A_descriptado = crypto::rsa_decrypt(token_A, chave_privada_cliente);
		}
		pacote_request_challenge = new Pacote(REQUEST_CHALLENGE);
		network::write(fd, pacote_request_challenge->getPacote());

		array::destroy(cabecalho);
		if((cabecalho = network::read(fd, 4)) == nullptr){
			cout << "Erro na leitura do pacote" << endl;
		}else{
			pacote_challenge_union.tamanho_byte[0] = cabecalho->data[0];
			pacote_challenge_union.tamanho_byte[1] = cabecalho->data[1];
			pacote_challenge_union.tamanho_byte[2] = cabecalho->data[2];
			pacote_challenge_union.tamanho_byte[3] = cabecalho->data[3];
			if((pacote_challenge = network::read(fd, pacote_challenge_union.tamanho)) == nullptr){
				cout << "Erro na leitura do pacote" << endl;	
			}else{
				cout << "Imprimindo conteudo do pacote recebido CHALLENGE " << *pacote_challenge << endl;
				sleep(2);
				printf("\n");
				pacote_challenge_auth = new Pacote(pacote_challenge, CHALLENGE);
				array::array *M_criptado = pacote_challenge_auth->getValue();
				M_descriptado = crypto::aes_decrypt(M_criptado, token_A_descriptado, chave_S_descriptada);
			}
		}
		pacote_authenticate = new Pacote(AUTHENTICATE, M_descriptado);
		network::write(fd, pacote_authenticate->getPacote());
		array::destroy(cabecalho);
		if((cabecalho = network::read(fd, 4)) == nullptr){
			cout << "Erro na leitura do pacote" << endl;
		}else{
			pacote_authenticated_union.tamanho_byte[0] = cabecalho->data[0];
			pacote_authenticated_union.tamanho_byte[1] = cabecalho->data[1];
			pacote_authenticated_union.tamanho_byte[2] = cabecalho->data[2];
			pacote_authenticated_union.tamanho_byte[3] = cabecalho->data[3];
			if((pacote_authenticated = network::read(fd, pacote_authenticated_union.tamanho)) == nullptr){
				cout << "Erro na leitura do pacote" << endl;	
			}else{
				cout << "Imprimindo conteúdo do pacote recebido AUTHENTICATED: " << *pacote_authenticated << endl;
				sleep(2);
				printf("\n");

				pacote_authenticated_auth = new Pacote(pacote_authenticated, AUTHENTICATED);
				array::array *token_T = pacote_authenticated_auth->getValue();
				//cout << "token t encriptado" << *token_T <<endl;
				token_T_descriptado = crypto::aes_decrypt(token_T, token_A_descriptado, chave_S_descriptada);
				//cout << "token t descriptado" << endl;
			}
		}
	printf("\n\n\n");
	cout << "PROTOCOLO DE REQUISICÃO!" << endl;
	cout << "ID DO OBJETO"<< *ID_objeto << endl;
	cout << "TOKEN T descriptado"<< *token_T_descriptado << endl;
	cout << "CHAVE S descriptado"<< *chave_S_descriptada << endl;
	array::array *ID_objeto_criptado = crypto::aes_encrypt(ID_objeto, token_T_descriptado, chave_S_descriptada);
	cout << "objeto encriptado" << *ID_objeto_criptado << endl;
	pacote_request_object = new Pacote(REQUEST_OBJECT, ID_objeto_criptado);
	network::write(fd, pacote_request_object->getPacote());
	array::destroy(cabecalho);
		if((cabecalho = network::read(fd, 4)) == nullptr){
			cout << "erro na leitura do pacote" << endl;
		}else{
			cout << "cabecalho = " << *cabecalho << endl;
			pacote_objeto_union.tamanho_byte[0] = cabecalho->data[0];
			pacote_objeto_union.tamanho_byte[1] = cabecalho->data[1];
			pacote_objeto_union.tamanho_byte[2] = cabecalho->data[2];
			pacote_objeto_union.tamanho_byte[3] = cabecalho->data[3];

			if((pacote_objeto = network::read(fd, pacote_objeto_union.tamanho)) == nullptr){
				cout << "erro na leitura do pacote" << endl;	
			}else{
				cout << "Imprimindo conteudo do pacote recebido OBJETO: " << *pacote_objeto << endl;
				sleep(2);
				printf("\n");

				pacote_objeto_auth = new Pacote(pacote_objeto, OBJECT);
				array::array *objeto_encriptado = pacote_objeto_auth->getValue();

				cout << "OBJETO encriptado "<< *objeto_encriptado << endl;
				objeto_descriptado = crypto::aes_decrypt(objeto_encriptado, token_T_descriptado, chave_S_descriptada);
				cout << "TOKEN T descriptado" << endl;
			}
		}	
	}	
	ofstream imagem;
	imagem.open ("Files/imagem.jpg", ios::binary);
	if( !imagem ){
		cout << "Erro ao criar o arquivo";
		return 1;
	}else{
		cout << "Criando o OBJECT, baixando IMAGEM!\n" << endl;
		sleep(2);
		for (int i = 0; i < ((int)objeto_descriptado->length); i++){
			imagem << objeto_descriptado->data[i];
		}
	}
	imagem.close();
	return 0;
}